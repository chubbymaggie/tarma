module Tarma
  module Exceptions
    class OnlyNilInChanges < StandardError
    end
  end
end
