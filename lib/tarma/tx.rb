module Tarma
  class Tx
    include Comparable
    attr_reader :index, :id, :date, :items

    def initialize(index,id,date = nil,items)
      @index = index
      @id    = id
      @date  = date
      @items = items
    end

    def size
      self.items.size
    end

    def <=> other
      self.index <=> other.index
    end

    def to_i
      self.index.to_i
    end

    def to_s
      self.id.to_s
    end

    def to_a
      self.items.to_a
    end
  end
end
