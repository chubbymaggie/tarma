Thank you for checking out the tarma repository!

Here you will find the (ruby) implementation of evolutionary coupling based change impact analysis algorithms,
presented in the paper "Generalizing the Analysis of Evolutionary Coupling for Software Change Impact Analysis".

